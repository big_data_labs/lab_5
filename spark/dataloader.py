import configparser
import pathlib

from pyspark.sql import SparkSession, DataFrame

from spark import config


class DatasetLoader:
    def __init__(
            self,
            spark_session: SparkSession,
    ):
        self.config = configparser.ConfigParser()
        self.config.read('config.ini')
        self.spark_session = spark_session

    def load_dataset(self) -> DataFrame:
        path = str(config.DATA_DIR / 'dataset.csv')
        dataset = self.spark_session.read.csv(
            path,
            header=True,
            inferSchema=True,
            sep='\t',
        )
        dataset.fillna(value=0)
        return dataset
