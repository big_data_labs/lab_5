import configparser
from pathlib import Path

ROOD_DIR = Path(__file__).parent.parent
DATA_DIR = ROOD_DIR / "data"

CONFIG = configparser.ConfigParser()
CONFIG.read(ROOD_DIR / 'config.ini')
